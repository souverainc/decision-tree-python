from sklearn import tree

# [height, weight, shoe size]
x = [[180,90,46],[160,45,36],[177,88,45],[178,80,40],[160,65,38]]
y = ['male','female','male','male','female']


clf = tree.DecisionTreeClassifier()

clf = clf.fit(x,y)

prediction = clf.predict([[182,90,44]])

print prediction